/******************************************************************************
*
* Bao Son Le
* 2020/03/03
* 
* Simple assembly exercise without startup code
*
******************************************************************************/

    #include "asm_compiler.h"

    THUMB2
    
    FILE_START

    /* Vector table
     *
     * Align 128 bytes (2^7)
     * Why?
     * Because VTOR[TBLOFF] is avaiable from bit 31 - bit 7.
     *
     * Following ARM v7-M Architecture Reference Manual:
     *
     * The Vector table must be naturally aligned to a power of two
     * whose alignment value is greater than or equal to (Number of
     * Exceptions supported x 4), with a minimum alignment of 128 bytes
     *
     * [Link](https://static.docs.arm.com/ddi0403/eb/DDI0403E_B_armv7m_arm.pdf)
     * Part B1.5.3 - pg. B1-581
     */
    SECTION_DATA_ALIGN(isr_vector, 7)
    EXPORT g_pfnVectors

    TYPE(g_pfnVectors, object)

LABEL(g_pfnVectors)
    WORD _estack
    WORD Reset_Handler+1

    ALLOC_BYTES(1016)

    SIZE(g_pfnVectors, .-g_pfnVectors)


    /* Main program */
    SECTION_EXEC(text.SimpleProject)
    ALIGN_BYTES_4
    EXPORT Reset_Handler
    EXPORT Main

LABEL(Reset_Handler)
    EOR     R0,  R0
    EOR     R1,  R1
    EOR     R2,  R2
    EOR     R3,  R3
    EOR     R4,  R4
    EOR     R5,  R5
    EOR     R6,  R6
    EOR     R7,  R7
    EOR     R8,  R8
    EOR     R9,  R9
    EOR     R10, R10
    EOR     R11, R11
    EOR     R12, R12

    TYPE(Main, function)
LABEL(Main)
    LDR.W   R5, =0xFEED0000
    MOV     R3, #0xBABE

    ADD     R6, R5, R3

    LTORG

LABEL(Infinite_Loop)
    B       Infinite_Loop

    FILE_END

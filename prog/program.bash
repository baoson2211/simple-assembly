#!/bin/bash

JLINK_EXE=$(which JLinkExe)

$JLINK_EXE -autoconnect 1 \
           -if SWD \
           -speed 1000 \
           -device STM32F407VG \
           -ExitOnError 1 \
           -CommanderScript $(dirname "${0}")/Probe.Jlink

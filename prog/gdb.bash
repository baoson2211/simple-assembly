#!/bin/bash

arg=$1

if [ "$arg" == "" ]
then
  arg=client
fi

case $arg in
    "s" | "server")
       JLINK_GDB_SVR=$(which JLinkGDBServer)

       $JLINK_GDB_SVR -if SWD \
                      -speed 1000 \
                      -device STM32F407VG
       ;;
    "c" | "client")
       TARGET=$(basename "$PWD")
       GDB=$(which arm-none-eabi-gdb)

       $GDB
       ;;
    *)
       echo "Please select one in the options: \"client\" or \"server\"."
       echo "If you don't pass any options, \"client\" is chosen as default."
       ;;
esac


/******************************************************************************
*
* Bao Son Le
* 2020/03/03
*
* Redefine directive of specific compilers common header
*
******************************************************************************/
#ifndef ASM_COMPILER_H
#define ASM_COMPILER_H

#ifdef __GNUC__
    #define THUMB  .thumb
    #define THUMB2 .thumb ; .syntax unified

    #define TYPE(name, ident) .type name, %ident

    #define OPCODE_START /*opcode start*/
    #define OPCODE_END   /*opcode end*/

    #define RESERVE_BYTES(N)      .block  N
    #define RESERVE_ZERO_BYTES(N) .blockz N

    #define LABEL(label) label##:
    #define CODE  .code

    #define EQU   .equ

    #define WORD  .long
    #define HALF  .half
    #define SHORT .short

    #define IMPORT .import
    #define EXPORT .global
    #define EXTERN .global

    #define ALIGN_POWER_OF_TWO(POWER) .align POWER
    #define LTORG .ltorg
    
    #define ALLOC_BYTES(N) .space N
    
    /* has to be at the beginning of a line */
    #define SET(NAME,VALUE) .set NAME, VALUE
    /* has to be at the beginning of a line */
    #define DEFINE(NAME,VALUE) .set NAME, VALUE

    #define SECTION_EXEC(SECTION_NAME) .section .SECTION_NAME,"ax"
    #define SECTION_DATA(SECTION_NAME) .section .SECTION_NAME,"aw"
    #define SECTION_CONST(SECTION_NAME) .section .SECTION_NAME,"a"
    #define SECTION_EXEC_W(SECTION_NAME) .section .SECTION_NAME,"awx"
    #define SECTION_DATA_UNINIT(SECTION_NAME) .section .SECTION_NAME,"aw"
    
    #define SECTION_EXEC_ALIGN(SECTION_NAME, ALIGNMENT) SECTION_EXEC(SECTION_NAME) ; ALIGN_POWER_OF_TWO(ALIGNMENT)
    #define SECTION_DATA_ALIGN(SECTION_NAME, ALIGNMENT) SECTION_DATA(SECTION_NAME) ; ALIGN_POWER_OF_TWO(ALIGNMENT)
    #define SECTION_CONST_ALIGN(SECTION_NAME, ALIGNMENT) SECTION_CONST(SECTION_NAME) ; ALIGN_POWER_OF_TWO(ALIGNMENT)
    #define SECTION_EXEC_W_ALIGN(SECTION_NAME, ALIGNMENT) SECTION_EXEC_W(SECTION_NAME) ; ALIGN_POWER_OF_TWO(ALIGNMENT)
    #define SECTION_DATA_UNINIT_ALIGN(SECTION_NAME, ALIGNMENT) SECTION_DATA_UNINIT(SECTION_NAME) ; ALIGN_POWER_OF_TWO(ALIGNMENT)

    #define SIZE(NAME,EXPRESSION)   .size NAME, EXPRESSION

    #define COND_INSTRUCTION(INSTRUCTION,CONDITION,WIDTH,...) INSTRUCTION##CONDITION##.##WIDTH __VA_ARGS__
    
    #define FILE_START
    #define FILE_END   .end

    #define APSR_nzcvq APSR_nzcvq

#endif

#if defined (__GNUC__)
#define BYTES_2   1
#define BYTES_4   2
#define BYTES_8   3
#define BYTES_16  4
#define BYTES_32  5
#define BYTES_64  6
#define BYTES_128 7

#define ALIGN_BYTES_2   ALIGN_POWER_OF_TWO(1)
#define ALIGN_BYTES_4   ALIGN_POWER_OF_TWO(2)
#define ALIGN_BYTES_8   ALIGN_POWER_OF_TWO(3)
#define ALIGN_BYTES_16  ALIGN_POWER_OF_TWO(4)
#define ALIGN_BYTES_32  ALIGN_POWER_OF_TWO(5)
#define ALIGN_BYTES_64  ALIGN_POWER_OF_TWO(6)
#define ALIGN_BYTES_128 ALIGN_POWER_OF_TWO(7)

/**
* use this to write 32bit opcode. This is one (HIGH) is the first half and it should go first.
*/
#define OPCODE32_HIGH(DATA) SHORT DATA
/**
* use this to write 32bit opcode. This is one (LOW) is the second half and it should go after OPCODE32_HIGH.
*/
#define OPCODE32_LOW(DATA) SHORT DATA
/**
* use this to write 16bit opcode.
*/
#define OPCODE16(DATA) SHORT DATA
#endif 

/*==============================================================================*/
/* C/C++ compiler macros */
#if defined (__GNUC__)
/* C/C++ compiler macros for placing a variable in an assembly defined section */
#define PLACE_IN_SECTION_HELPER(SECTION_NAME) __attribute__ (( section(#SECTION_NAME) ))

#define PLACE_IN_SECTION(SECTION_NAME) PLACE_IN_SECTION_HELPER(.SECTION_NAME) 

#define INLINE inline
#endif /* __GNUC__ */

#endif /* #ifndef ASM_COMPILER_H */


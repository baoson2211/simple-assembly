# Demo automation test an Assembly program
Demo using GDB for automation test a Assembly program with GNU ARM Toolchain.

Documentation:

* GDB: [https://sourceware.org/gdb/current/onlinedocs/gdb](https://sourceware.org/gdb/current/onlinedocs/gdb)

### 1. Structure of Directory
```
├── build                       => auto generate after build (Note: be delete after clean, only present after build)
├── S                           => a folder contains assembly files
│   └── main.s
├── h                           => a folder contains header files
│   └── asm_compiler.h
├── linker                      => a folder contains linker file
│   └── STM32F407VGTx_FLASH.ld
├── prog                        => a folder contains the scripts for program and debug
│   ├── Probe.Jlink
│   ├── gdb.bash
│   ├── gdb.script
│   └── program.bash
├── Makefile                    => Makefile
└── README.md                   => this file
```
### 2. Using
* Build: ```make```
* Program: go to ```prog``` folder and run ```./program.bash```
* Debug: go to ```prog``` folder and run ```arm-none-eabi-gdb -x gdb.script```. Please make sure JLinkGDBServer has been started.
